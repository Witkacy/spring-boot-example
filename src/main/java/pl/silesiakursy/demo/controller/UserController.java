package pl.silesiakursy.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.silesiakursy.demo.converter.UserDTOToUserConverter;
import pl.silesiakursy.demo.converter.UserToUserDTOConverter;
import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;
import pl.silesiakursy.demo.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/")
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = userRepository.findAll();

        return ResponseEntity.status(HttpStatus.OK).body(users);
    }

    @PostMapping("/")
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        User userToSave = UserDTOToUserConverter.converter(userDTO);
        User savedUser = userRepository.save(userToSave);
        UserDTO convertedUserDTO = UserToUserDTOConverter.convert(savedUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(convertedUserDTO);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable Integer id) {

        if(!userRepository.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        if(!id.equals(user.getId())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Id does not match");
        }

        User savedUser = userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK).body(savedUser);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id) {

        if(!userRepository.existsById(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        userRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Integer id ) {

        Optional<User> user = userRepository.findById(id);

        if(user.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(user.get());
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
