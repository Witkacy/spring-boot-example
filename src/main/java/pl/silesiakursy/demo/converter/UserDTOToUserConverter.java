package pl.silesiakursy.demo.converter;

import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;

public class UserDTOToUserConverter {

    public static User converter(UserDTO userDTO){
        User user = new User();
        user.setId(UserDTO.getId());
        user.setEmail(UserDTO.getEmail());
        user.setFirstName(UserDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        return user;
    }
}
