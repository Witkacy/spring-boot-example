package pl.silesiakursy.demo.converter;

import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;

public class UserToUserDTOConverter {

    public static UserDTO convert(User user){
        UserDTO UserDTO = new UserDTO();
        UserDTO.setId(user.getId());
        UserDTO.setEmail(user.getEmail());
        UserDTO.setFirstName(user.getFirstName());
        UserDTO.setLastName(user.getLastName());
        return UserDTO;
    }


}
